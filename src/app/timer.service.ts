import { Injectable } from '@angular/core';
import { timer } from 'rxjs/observable/timer';

@Injectable()

export class TimerService {
    public observ = timer(0, 1000);

    runTimer () {
        return this.observ;
    }
}
