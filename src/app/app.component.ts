import { Component, OnInit } from '@angular/core';
import { TimerService } from './timer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  public hours: string | number = '--';
  public minutes: string | number = '--';
  public second: string | number = '--';
  public subscription;

  constructor (
      private timerService: TimerService
  ) {}

  ngOnInit () {

  }

  start () {
      this.subscription = this.timerService.runTimer().subscribe(res => {
          this.timer(res);
      });
  }

  stop () {
      this.unsub();
  }

  wait () {

  }

  reset () {
    this.unsub();
    this.hours = this.minutes = this.second = '--';
  }

  unsub () {
      this.subscription.unsubscribe();
  }

  private timer (time: number): void {
      this.hours = this.AddZero(Math.floor((time / 60) / 60));
      this.minutes = this.AddZero((Math.floor(time / 60)) % 60);
      this.second = this.AddZero(time % 60);
  }

  private AddZero(elem: any): string | number {
      return elem <= 9 ? '0' + elem : elem;
  }

}
